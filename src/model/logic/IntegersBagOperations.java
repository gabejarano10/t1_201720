package model.logic;

import java.util.Iterator;

import model.data_structures.IntegersBag;

public class IntegersBagOperations {

	
	
	public double computeMean(IntegersBag bag){
		double mean = 0;
		int length = 0;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				mean += iter.next();
				length++;
			}
			if( length > 0) mean = mean / length;
		}
		return mean;
	}
	
	
	public int getMax(IntegersBag bag){
		int max = Integer.MIN_VALUE;
	    int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( max < value){
					max = value;
				}
			}
			
		}
		return max;
	}
	public int getMinimo(IntegersBag bag)
	{
		int min = Integer.MAX_VALUE;
		int value;
		if( bag!=null)
		{
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext())
			{
				value= iter.next();
				if(min>value)
				{
					min=value;
				}
			}
		}
		return min;
	}
	public double productoria(IntegersBag bag)
	{
		int value=1;
		if(bag!=null)
		{
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext())
			{
				value*=iter.next();
			}
		}
		return value;
	}
	public double sumatoria(IntegersBag bag)
	{
		int value=0;
		if(bag!=null)
		{
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext())
			{
				value+=iter.next();
			}
		}
		return value;
	}
	
}
